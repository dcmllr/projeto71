import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    public static void main(String[] args) {
    Time time1 = new Time();
    JogadorComparator a = new JogadorComparator(true, true, false);
    Scanner scanner = new Scanner (System.in);
    int numJogadores, nJogador;
    String jog = new String();
    System.out.print("Digite aqui o numero de jogadores que deseja por: ");
    if(scanner.hasNextInt()){
        numJogadores = scanner.nextInt();
         List<Jogador> v= new ArrayList<>();

            for(int i=0; i<numJogadores; i++){
                if(!scanner.hasNextInt()){
                    i--;
                    System.out.println("Digite um numero valido!");
                }
                else{
                    nJogador = scanner.nextInt();
                    if(nJogador<0){
                        System.out.println("Digite um numero valido!");
                        i--;
                    }else{
                    jog = scanner.next();
                    v.add(new Jogador(nJogador, jog));
                    }
                }
               
            }
                    
             v.sort(new JogadorComparator(true, true, false));
             
             System.out.println(v);
     
             nJogador = 3;
    
    while(nJogador!=0){
                System.out.println("Digite o numero do jogador");
                int x=0;
                
                if(scanner.hasNextInt()){
                    nJogador = scanner.nextInt();
                    if(nJogador<0){
                        System.out.println("Digite um numero valido!");
                        continue;
                    }

                    if(nJogador==0)
                        break;

                    else{
                        System.out.println("Digite o nome do jogador");
                        jog = scanner.next();

                        for(int j=0; j<v.size(); j++){
                            if(nJogador == v.get(j).getNumero()){
                                v.remove(j);
                                v.add(j, new Jogador(nJogador, jog));
                                break;
                            }   
                            else if(nJogador<v.get(0).getNumero()){
                                v.add(0, new Jogador(nJogador, jog));
                                break;
                            }
                            else if(nJogador>v.get(j).getNumero()){
                                    x=j;
                             }
                        }
                    if(x!=0)
                        v.add(x+1, new Jogador(nJogador, jog));
                    System.out.println(v);
                    }
            }
        }
    }
    }
}